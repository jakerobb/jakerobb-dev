import { Component, OnInit } from '@angular/core';
import * as moment from "moment";
import {Router} from "@angular/router";

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  router: Router;
  today: moment.Moment = moment();
  birthDate: moment.Moment = moment("1980-09-14");
  anniversaryDate: moment.Moment = moment("2004-09-11");
  birthdayIsToday: boolean = this.isTodayAnniversaryOf(this.birthDate);
  ageInYears: number = this.yearsSince(this.birthDate);
  elizabethAgeInYears: number = this.yearsSince(moment("2014-01-18"));
  ameliaAgeInYears: number = this.yearsSince(moment("2018-07-01"));
  anniversaryIsToday: boolean = this.isTodayAnniversaryOf(this.anniversaryDate);
  marriedYears: number = this.yearsSince(this.anniversaryDate);

  private isTodayAnniversaryOf(historicalDate: moment.Moment): boolean {
    return moment(this.today).format("MMDD") === historicalDate.format("MMDD");
  }

  private yearsSince(historicalDate: moment.Moment): number {
    return moment(this.today).diff(moment(historicalDate), 'years')
  }

  constructor(router: Router) {
    this.router = router;
  }

  ngOnInit() {
  }
}
