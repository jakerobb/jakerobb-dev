import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MenuComponent} from './menu/menu.component';
import {HomeComponent} from './home/home.component';
import {AboutComponent} from './about/about.component';
import {ElsewhereComponent} from "./about/elsewhere/elsewhere.component";
import {BlogComponent} from './blog/blog.component';
import {CarsComponent} from './cars/cars.component';
import {CamaroComponent} from './cars/camaro/camaro.component';
import {GrandNationalComponent} from './cars/grand-national/grand-national.component';
import {CalculatorsComponent} from './cars/calculators/calculators.component';
import {TrackFuelEconomyComponent} from './cars/calculators/track-fuel-economy/track-fuel-economy.component';
import {SpeedAndGearingComponent} from './cars/calculators/speed-and-gearing/speed-and-gearing.component';
import {WheelAndTireSizeComponent} from "./cars/calculators/wheel-and-tire-size/wheel-and-tire-size.component";
import {MetaComponent} from './meta/meta.component';


const carCalculatorRoutes: Routes = [
  {path: 'track-fuel-economy', component: TrackFuelEconomyComponent},
  {path: 'speed-and-gearing', component: SpeedAndGearingComponent},
  {path: 'wheel-and-tire-size', component: WheelAndTireSizeComponent}
];

const aboutRoutes: Routes = [
  {path: 'elsewhere', component: ElsewhereComponent}
];

const carRoutes: Routes = [
  {path: 'camaro', component: CamaroComponent},
  {path: 'grand-national', component: GrandNationalComponent},
  {path: 'calculators', component: CalculatorsComponent, children: carCalculatorRoutes}
];

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'about', component: AboutComponent, children: aboutRoutes},
  {path: 'blog', component: BlogComponent},
  {path: 'cars', component: CarsComponent, children: carRoutes},
  {path: 'meta', component: MetaComponent},
  {path: '**', component: HomeComponent},
  {path: "", component: MenuComponent, outlet: "menu"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
