import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { TwoDecimalDirective } from './two-decimal.directive';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { BlogComponent } from './blog/blog.component';
import { CarsComponent } from './cars/cars.component';
import { TrackFuelEconomyComponent } from './cars/calculators/track-fuel-economy/track-fuel-economy.component';
import { SpeedAndGearingComponent } from './cars/calculators/speed-and-gearing/speed-and-gearing.component';
import { MenuComponent } from './menu/menu.component';
import { CamaroComponent } from './cars/camaro/camaro.component';
import { GrandNationalComponent } from './cars/grand-national/grand-national.component';
import { CalculatorsComponent } from './cars/calculators/calculators.component';
import { MetaComponent } from './meta/meta.component';
import { ElsewhereComponent } from './about/elsewhere/elsewhere.component';
import { WheelAndTireSizeComponent } from './cars/calculators/wheel-and-tire-size/wheel-and-tire-size.component';
import { AbsoluteValuePipe } from './absolute-value.pipe';
import { StorageModule } from '@ngx-pwa/local-storage';
import { LZStringModule, LZStringService } from 'ng-lz-string';

@NgModule({
  declarations: [
    AppComponent,
    TwoDecimalDirective,
    HomeComponent,
    AboutComponent,
    BlogComponent,
    CarsComponent,
    TrackFuelEconomyComponent,
    SpeedAndGearingComponent,
    MenuComponent,
    CamaroComponent,
    GrandNationalComponent,
    CalculatorsComponent,
    MetaComponent,
    ElsewhereComponent,
    WheelAndTireSizeComponent,
    AbsoluteValuePipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    StorageModule.forRoot({
      IDBNoWrap: true,
    }),
    LZStringModule
  ],
  providers: [
    // Specify the service in the providers section
    LZStringService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
