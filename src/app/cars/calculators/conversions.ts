export function inchesToMillimeters(inches: number): number {
  return inches * 25.4;
}

export function millimetersToInches(millimeters: number) {
  return millimeters / 25.4;
}

export function circumferenceFromDiameter(diameter: number) {
  return diameter * Math.PI;
}

