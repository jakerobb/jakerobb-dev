import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpeedAndGearingComponent } from './speed-and-gearing.component';

describe('SpeedAndGearingComponent', () => {
  let component: SpeedAndGearingComponent;
  let fixture: ComponentFixture<SpeedAndGearingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpeedAndGearingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpeedAndGearingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
