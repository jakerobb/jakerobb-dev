import {Component, ElementRef, Injectable, OnInit, QueryList, ViewChildren} from '@angular/core';
import {CarSpec, SpeedRating, GearSpec} from "./speedAndGearingInput";
import {MphEntry, MphRow, SpeedAndGearingOutput} from "./speedAndGearingOutput";
import {animate, animateChild, query, stagger, style, transition, trigger} from "@angular/animations";
import {v4 as uuid} from 'uuid';
import { StorageMap } from '@ngx-pwa/local-storage';
import { LZStringModule, LZStringService } from 'ng-lz-string';

@Component({
  selector: 'app-speed-and-gearing',
  templateUrl: './speed-and-gearing.component.html',
  styleUrls: ['./speed-and-gearing.component.scss'],

  animations: [
    trigger('list', [
      transition(':enter', [
        query('@items', stagger(150, animateChild()))
      ]),
    ]),
    trigger('items', [
      transition(':enter', [
        style({transform: 'scale(0, 1)', opacity: 1, 'transform-origin': 'left'}),
        animate('400ms ease-in', style({transform: 'scale(1, 1)', opacity: 1, 'transform-origin': 'left'}))
      ]),
      transition(':leave', [
        animate('400ms ease-in', style({transform: 'scale(0, 1)', opacity: 1, 'transform-origin': 'left'}))
      ])
    ])
  ]
})


@Injectable()
export class SpeedAndGearingComponent implements OnInit {

  static roundRpmToLandOnATableRow(rpm: number, carSpec: CarSpec, preferences: SpeedAndGearingPreferences) {
    return Math.round((rpm) / preferences.rpmInterval) * preferences.rpmInterval; // round to nearest value we'll actually calculate for
  }

  static computeTopGearCruiseRpm(gear: GearSpec, topGearCruiseMph: number) {
    return gear.getRpmForMph(topGearCruiseMph);
  }

  static computeMphTable(carSpec: CarSpec, gears: GearSpec[], preferences: SpeedAndGearingPreferences): MphRow[] {
    var table: MphRow[] = [];
    var avoidRpm = -1;
    if (preferences.avoidRpmEnabled) {
      avoidRpm = SpeedAndGearingComponent.roundRpmToLandOnATableRow(preferences.avoidRpm, carSpec, preferences)
    }
    var roundedMinimumRpm = SpeedAndGearingComponent.roundRpmToLandOnATableRow(carSpec.minimumRpm, carSpec, preferences);
    var startingRpm = roundedMinimumRpm;
    if (roundedMinimumRpm !== carSpec.minimumRpm) {
      this.addRowForRpm(carSpec.minimumRpm, carSpec, gears, avoidRpm, table);
      if (startingRpm <= carSpec.minimumRpm) {
        startingRpm += preferences.rpmInterval;
      }
    }
    for (var rpm = startingRpm; rpm <= carSpec.maximumRpm; rpm += preferences.rpmInterval) {
      this.addRowForRpm(rpm, carSpec, gears, avoidRpm, table);
    }
    if (table[table.length - 1].rpm < carSpec.maximumRpm) {
      var roundedMaxRpm = SpeedAndGearingComponent.roundRpmToLandOnATableRow(carSpec.maximumRpm, carSpec, preferences);
      if (avoidRpm === roundedMaxRpm) {
        avoidRpm = carSpec.maximumRpm;
      }
      this.addRowForRpm(carSpec.maximumRpm, carSpec, gears, avoidRpm, table);
    }
    return table;
  }

  static addRowForRpm(rpm: number, carSpec: CarSpec, gears: GearSpec[], avoidRpm, table) {
    var mphByGear = [];
    for (var gearIndex = 0; gearIndex < gears.length; gearIndex++) {
      var gear: GearSpec = gears[gearIndex];
      let mph = gear.getMphForRpm(rpm);
      mphByGear.push(new MphEntry(gear, mph, carSpec.tireSpec.speedRating.isOverSpeed(mph)));
    }
    var mphRow = new MphRow(rpm, mphByGear, avoidRpm === rpm);
    // mphRow.print();
    table.push(mphRow);
  }

  static calculateOutput(carSpec: CarSpec, preferences: SpeedAndGearingPreferences): SpeedAndGearingOutput {
    var topGearCruiseRpm: number = -1000;
    var roundedTopGearCruiseRpm: number = -1000;
    if (preferences.topGearCruiseEnabled) {
      topGearCruiseRpm = SpeedAndGearingComponent.computeTopGearCruiseRpm(carSpec.topGear(), preferences.topGearCruiseMph);
      roundedTopGearCruiseRpm = SpeedAndGearingComponent.roundRpmToLandOnATableRow(topGearCruiseRpm, carSpec, preferences);
    }
    var mphTable = SpeedAndGearingComponent.computeMphTable(carSpec, carSpec.gears, preferences);
    return {
      carSpec: carSpec,
      topGearCruiseRpm: topGearCruiseRpm,
      roundedTopGearCruiseRpm: roundedTopGearCruiseRpm,
      mphTable: mphTable
    };
  }

  @ViewChildren('mphByRpmCanvas') canvasContexts:QueryList<ElementRef<HTMLCanvasElement>>;
  constructor(private storageMap: StorageMap, private lz: LZStringService, private elementRef: ElementRef) {
    this.sets = [];
    this.preferences = new SpeedAndGearingPreferences(100, true, 77, false, 2000, false);
    this.sets[0] = SpeedAndGearingSet.fromPreset(this,  "LS1 T56 Camaro SS / Trans Am WS6", this.preferences);
    let thisComponent = this;

    this.storageMap.get('preferences').subscribe((storedData: string) => {
      if (storedData !== undefined) {
        //console.log('loaded new speedAndGearing data from local storage: ' + storedData);
        thisComponent.loadPreferencesDataFromCompressedString(storedData);
      }
    });
    this.storageMap.get('sets').subscribe((storedData: string) => {
      if (storedData !== undefined) {
        //console.log('loaded new speedAndGearing data from local storage: ' + storedData);
        thisComponent.loadSetsDataFromCompressedString(storedData);
      }
    });
  }

  loadPreferencesDataFromCompressedString(compressedStoredData: string) {
    var storedData = this.lz.decompress(compressedStoredData);
    if (storedData === undefined) {
      storedData = compressedStoredData;
    }
    this.preferences = JSON.parse(storedData);
    this.onChangePreferences();
  }

  loadSetsDataFromCompressedString(compressedStoredData: string) {
    var storedData = this.lz.decompress(compressedStoredData);
    if (storedData === undefined) {
      storedData = compressedStoredData;
    }
    let storedSets: SpeedAndGearingSet[] = JSON.parse(storedData);
    this.sets = [];
    storedSets.forEach((storedSet: SpeedAndGearingSet) => {
      storedSet.component = this;
      this.sets.push(SpeedAndGearingSet.copy(storedSet, this.preferences));
      this.calculateAll();
    });
  }

  preferences: SpeedAndGearingPreferences;
  sets: SpeedAndGearingSet[];

  updatePreferencesInLocalStorage() {
    let data = this.getPreferencesDataAsCompressedString();
    //console.log('writing new prefs data to local storage' + data);
    this.storageMap.set('preferences', data).subscribe(() => {
      // console.log('wrote new prefs data to local storage')
    });
  }

  updateSetsInLocalStorage() {
    let data = this.getSetsDataAsCompressedString();
    //console.log('writing new sets data to local storage' + data);
    this.storageMap.set('sets', data).subscribe(() => {
      // console.log('wrote new sets data to local storage')
    });
  }

  private getPreferencesDataAsCompressedString() {
    let json = JSON.stringify(this.preferences);
    //console.log(json);
    let compressed = this.lz.compress(json);
    //console.log(json.length + ' bytes compressed down to ' + compressed.length);
    return compressed;
  }

  private getSetsDataAsCompressedString() {
    let json = JSON.stringify(this.sets, SpeedAndGearingComponent.replacer);
    //console.log(json);
    let compressed = this.lz.compress(json);
    //console.log(json.length + ' bytes compressed down to ' + compressed.length);
    return compressed;
  }

  static dropProperties = ['elementRef', 'nativeElement', 'lz', 'closed', '_parent', 'database', 'storageMap', 'component', 'observers', 'result', 'canvas',
    'canvasContext', 'canvasIsUpToDate', 'tireRevsPerEngineRev', 'mphAtMaxRpm', 'ratioSpreadFromPreviousGear', 'combinedFinalDriveRatio', 'shiftFromPreviousGearRpm',
    'gears', 'maxMph', 'plus', 'uniqueId', 'sectionWidthInches', 'sectionWidthMillimeters', 'tireDiameterInches', 'tireDiameterMillimeters', 'profileHeightInches',
    'profileHeightMillimeters', 'profileHeightPercentage', 'tireCircumferenceInches', 'tireCircumferenceMillimeters'];
  static replacer(key, value) {
    if (typeof value === 'function') {
      return undefined;
    }
    if (SpeedAndGearingComponent.dropProperties.indexOf(key) >= 0) {
      return undefined;
    }
    return value;
  }

  ngOnInit() {
    this.calculateAll();
  }

  ngAfterViewInit(): void {
    var i = 0;
    this.canvasContexts.changes.subscribe(newList => {
      var j = 0;
      newList.forEach(canvasElement => this.sets[j++].provideCanvas(canvasElement, this.preferences))
    });
    this.canvasContexts.forEach(canvasElement => this.sets[i++].provideCanvas(canvasElement, this.preferences));
  }

  calculateAll() {
    var preferences = this.preferences;
    this.sets.forEach(function (set) {
      set.calculate(preferences);
    });
    this.updateSetsInLocalStorage();
  }

  get allSpeedRatings() {
    return SpeedRating.all;
  }

  get allCarPresets() {
    return CarSpec.presets;
  }

  sortAlphaAscending = (a, b) => {
    return a.key.localeCompare(b.key);
  };

  indexIdentity(index: any, item: any) {
    return index;
  }

  uniqueId(index: any, item: any) {
    return item.uniqueId;
  }

  onChangePreferences(): void {
    this.updatePreferencesInLocalStorage();
    this.calculateAll();
  }

  addCar() {
    this.sets.push(SpeedAndGearingSet.fromPreset(this, "LS1 T56 Camaro SS / Trans Am WS6", this.preferences));
    this.updateSetsInLocalStorage();
    this.changeItemCount();
  }

  cloneCar(setIndex) {
    this.sets.push(SpeedAndGearingSet.copy(this.sets[setIndex], this.preferences));
    this.updateSetsInLocalStorage();
    this.changeItemCount();
  }

  removeCar(setIndex) {
    this.sets.splice(setIndex, 1);
    this.updateSetsInLocalStorage();
    this.changeItemCount();
  }

  changeItemCount() {
    this.elementRef.nativeElement.style.setProperty('--setCount', this.sets.length);
  }

  ordinalGearFromIndex(gearIndex) {
    let gearNumber = gearIndex + 1;
    if (gearNumber % 10 == 1 && gearNumber != 11) {
      return gearNumber + "st";
    }
    if (gearNumber % 10 == 2 && gearNumber != 12) {
      return gearNumber + "nd";
    }
    if (gearNumber % 10 == 3 && gearNumber != 13) {
      return gearNumber + "rd";
    }
    return gearNumber + "th";
  }
}

export class SpeedAndGearingPreferences {
  rpmInterval: number;
  topGearCruiseEnabled: boolean;
  topGearCruiseMph: number;
  avoidRpmEnabled: boolean;
  avoidRpm: number;
  renderAsTable: boolean;

  constructor(rpmInterval: number, topGearCruiseEnabled: boolean, topGearCruiseMph: number, avoidRpmEnabled: boolean, avoidRpm: number, renderAsTable: boolean) {
    this.rpmInterval = rpmInterval;
    this.topGearCruiseEnabled = topGearCruiseEnabled;
    this.topGearCruiseMph = topGearCruiseMph;
    this.avoidRpmEnabled = avoidRpmEnabled;
    this.avoidRpm = avoidRpm;
    this.renderAsTable = renderAsTable;
  }
}

export class SpeedAndGearingSet {
  component: SpeedAndGearingComponent;
  preset: string;
  carSpec: CarSpec;
  result: SpeedAndGearingOutput;
  showInputDetails: boolean;
  uniqueId: string;
  canvas: HTMLCanvasElement;
  canvasContext: CanvasRenderingContext2D;
  canvasIsUpToDate: boolean;

  constructor(component: SpeedAndGearingComponent, preset: string, carSpec: CarSpec, preferences: SpeedAndGearingPreferences) {
    this.component = component;
    this.preset = preset;
    this.carSpec = carSpec;
    this.result = SpeedAndGearingComponent.calculateOutput(carSpec, preferences);
    this.showInputDetails = true;
    this.uniqueId = uuid();
    this.canvasIsUpToDate = false;
  }

  provideCanvas(canvasElement: ElementRef, preferences: SpeedAndGearingPreferences) {
    this.consoleLog('provideCanvas');
    this.canvas = <HTMLCanvasElement> canvasElement.nativeElement;
    this.canvasContext = this.canvas.getContext('2d');
    this.updateCanvas(preferences);
  }

  updateCanvasIfNeeded(preferences: SpeedAndGearingPreferences) {
    this.consoleLog('updateCanvasIfNeeded');
    if (!this.canvasIsUpToDate) {
      this.updateCanvas(preferences);
    }
  }

  updateCanvas(preferences: SpeedAndGearingPreferences) {
    this.consoleLog('updateCanvas');
    if (this.canvas) {
      this.canvasContext.globalCompositeOperation = 'source-over'; // draw text on top of lines
      this.clear();

      // compute scale
      let fastestMph = this.carSpec.topSpeed();

      let mphTextMetrics = this.canvasContext.measureText(Math.round(fastestMph) + " mph");
      let rpmTextMetrics = this.canvasContext.measureText(Math.round(this.carSpec.maximumRpm) + " rpm");

      // todo: replace these with textMetrics once it's widely supported https://stackoverflow.com/questions/1134586/how-can-you-find-the-height-of-text-on-an-html-canvas
      let smallLineHeight = 12;
      let lineHeight = 16;
      let descenderHeight = 4;

      let bottomMargin = 50;
      let leftMargin = 50;
      let topMargin = lineHeight * 2;
      let rightMargin = 10 + Math.max(mphTextMetrics.width, rpmTextMetrics.width) / 2;
      let labelXOffset = 6;
      let labelYOffset = 4;
      let yAxisLabelOffset = 16;
      let tickOffset = 4;
      let tickLength = 8;
      let rpmIncrement = 200;
      let rpmLabelIncrement = 1000;
      let mphIncrement = 10;
      let mphLabelIncrement = 50;
      let originX = leftMargin;
      let originY = this.canvas.height - bottomMargin;

      let topOfMphScale = Math.ceil(fastestMph / mphIncrement) * mphIncrement;
      let topOfRpmScale = Math.ceil(this.carSpec.maximumRpm / 100) * 100;

      this.canvasContext.globalAlpha = 1;
      this.canvasContext.textAlign = 'left';

      // draw Y axis (rpm)
      this.canvasContext.beginPath();
      this.canvasContext.lineWidth = 1;
      this.canvasContext.strokeStyle = '#000000';
      this.canvasContext.moveTo(originX, originY);
      this.canvasContext.lineTo(originX, topMargin);
      this.canvasContext.stroke();
      this.canvasContext.closePath();


      // draw tickmarks on Y axis
      this.canvasContext.font = '12px sans-serif';
      for (var rpm = rpmIncrement; rpm <= topOfRpmScale; rpm += rpmIncrement) {
        var tickY = this.getYValueForRpm(rpm, topOfRpmScale, bottomMargin, topMargin, this.canvas.height);
        if (rpm % rpmLabelIncrement === 0) {
          var text = rpm + '';
          var textMetrics = this.canvasContext.measureText(text);
          this.canvasContext.beginPath();
          this.canvasContext.fillStyle = '#000000';
          this.canvasContext.fillText(text, originX - textMetrics.width - labelXOffset, tickY + labelYOffset);
          this.canvasContext.closePath();
          this.canvasContext.lineWidth = 3;
        } else {
          this.canvasContext.lineWidth = 1;
        }
        this.canvasContext.beginPath();
        this.canvasContext.strokeStyle = '#000000';
        this.canvasContext.moveTo(originX - tickOffset, tickY);
        this.canvasContext.lineTo(originX - tickOffset + tickLength, tickY);
        this.canvasContext.stroke();
        this.canvasContext.closePath();
      }

      // draw X axis (mph)
      this.canvasContext.beginPath();
      this.canvasContext.lineWidth = 1;
      this.canvasContext.strokeStyle = '#000000';
      this.canvasContext.moveTo(originX, originY);
      this.canvasContext.lineTo(this.canvas.width - rightMargin, originY);
      this.canvasContext.stroke();
      this.canvasContext.closePath();

      // draw tickmarks on X axis
      this.canvasContext.font = '12px sans-serif';
      for (var mph = mphIncrement; mph <= topOfMphScale; mph += mphIncrement) {
        var tickX = this.getXValueForMph(mph, topOfMphScale, leftMargin, rightMargin, this.canvas.width);
        if (mph % mphLabelIncrement === 0) {
          var text = mph + '';
          var textMetrics = this.canvasContext.measureText(text);
          this.canvasContext.fillStyle = '#000000';
          this.canvasContext.fillText(text, tickX - textMetrics.width / 2, originY + yAxisLabelOffset);
          this.canvasContext.lineWidth = 2;
        } else {
          this.canvasContext.lineWidth = 1;
        }
        this.canvasContext.beginPath();
        this.canvasContext.strokeStyle = '#000000';
        this.canvasContext.moveTo(tickX, originY - tickOffset);
        this.canvasContext.lineTo(tickX, originY - tickOffset + tickLength);
        this.canvasContext.stroke();
        this.canvasContext.closePath();
      }

      this.canvasContext.textAlign = 'center';
      // draw tire speed rating limit
      let xValueForMaxMph = this.getXValueForMph(this.carSpec.tireSpec.speedRating.maxMph, topOfMphScale, leftMargin, rightMargin, this.canvas.width);
      if (!this.carSpec.tireSpec.speedRating.plus) {
        this.canvasContext.globalCompositeOperation = 'destination-over'; // draw lines beneath text
        this.canvasContext.beginPath();
        this.canvasContext.lineWidth = 1;
        this.canvasContext.strokeStyle = '#cccccc';
        this.canvasContext.setLineDash([7, 5]);
        this.canvasContext.moveTo(xValueForMaxMph, originY);
        this.canvasContext.lineTo(xValueForMaxMph, topMargin);
        this.canvasContext.stroke();
        this.canvasContext.closePath();
        this.canvasContext.globalCompositeOperation = 'source-over'; // draw text on top of lines
        this.canvasContext.setLineDash([]);
      } else {
        let xValueForRightSideOfShadedArea = this.getXValueForMph(topOfMphScale, topOfMphScale, leftMargin, rightMargin, this.canvas.width);
        let yCoordinateAtRedline = this.getYValueForRpm(topOfRpmScale, topOfRpmScale, bottomMargin, topMargin, this.canvas.height);
        let width = xValueForRightSideOfShadedArea - xValueForMaxMph;
        let height = originY - yCoordinateAtRedline;
        this.canvasContext.globalAlpha = 0.1;
        this.canvasContext.fillStyle = '#cc0000';
        this.canvasContext.fillRect(xValueForMaxMph, topMargin, width, height);
        this.canvasContext.globalAlpha = 1;
      }

      // draw lines for each gear
      let colors = ['#ff0000', '#00cc00', '#0000ff', '#ffaa00', '#ff00ff', '#00cccc', '#880088', '#888800', '#ff0088', '#888888'];
      var textMetrics: TextMetrics;
      var maximumXValueForRightBoundOfIdleMphTextByLine: number[] = [0];
      var idleMphTextVerticalOffsetLines: number;
      var needsAnotherPass: boolean = true;
      let textPadding = 5;

      var lastMph = this.carSpec.gears[0].getMphForRpm(this.carSpec.minimumRpm);
      // plot the lines for each gear
      var previousGearWasSpeedLimited = false;
      this.carSpec.gears.forEach((gear: GearSpec, gearIndex: number) => {
        let xValueForLastMph = this.getXValueForMph(lastMph, topOfMphScale, leftMargin, rightMargin, this.canvas.width);
        let rpmAtBeginningOfGear = gear.getRpmForMph(lastMph);
        let yValueForRpmAtLastMph = this.getYValueForRpm(rpmAtBeginningOfGear, topOfRpmScale, bottomMargin, topMargin, this.canvas.height);
        var mphAtRedlineInThisGear = gear.getMphForRpm(this.carSpec.maximumRpm);

        this.canvasContext.font = 'bold 14px sans-serif';
        this.canvasContext.fillStyle = colors[gearIndex % colors.length];

        let idleRpm: number = this.carSpec.minimumRpm;
        let mphAtIdleInThisGear: number = gear.getMphForRpm(this.carSpec.minimumRpm);
        let xValueForMphAtIdleInThisGear: number = this.getXValueForMph(mphAtIdleInThisGear, topOfMphScale, leftMargin, rightMargin, this.canvas.width);
        let yValueForIdleRpm: number = this.getYValueForRpm(idleRpm, topOfRpmScale, bottomMargin, topMargin, this.canvas.height);

        let rpmAtMaxSpeedInThisGear: number;
        let speedLimited: boolean;
        if (mphAtRedlineInThisGear > fastestMph) {
          speedLimited = true;
          mphAtRedlineInThisGear = fastestMph;
          rpmAtMaxSpeedInThisGear = gear.getRpmForMph(fastestMph);
        } else {
          speedLimited = false;
          rpmAtMaxSpeedInThisGear = topOfRpmScale;
        }

        let xValueForMphAtRedlineInThisGear = this.getXValueForMph(mphAtRedlineInThisGear, topOfMphScale, leftMargin, rightMargin, this.canvas.width);
        let yCoordinateAtMaxRpmInThisGear = this.getYValueForRpm(rpmAtMaxSpeedInThisGear, topOfRpmScale, bottomMargin, topMargin, this.canvas.height);
        this.consoleLog(gearIndex + ': ' + xValueForLastMph + "," + yValueForRpmAtLastMph + ' to ' + xValueForMphAtRedlineInThisGear + ',' + yCoordinateAtMaxRpmInThisGear);

        if (this.carSpec.minimumRpm > 0) {
          // since it's not zero, label the MPH at idle in this gear
          this.canvasContext.globalCompositeOperation = 'source-over'; // draw text on top of lines
          text = Math.round(mphAtIdleInThisGear) + ' mph';
          this.canvasContext.font = '10px sans-serif';
          textMetrics = this.canvasContext.measureText(text);

          // compute a Y coordinate that won't overlap with a previous gear's label, then the corresponding X coordinate
          idleMphTextVerticalOffsetLines = 0;
          var xValue = xValueForMphAtIdleInThisGear;
          var yValue = yValueForIdleRpm - ((idleMphTextVerticalOffsetLines - 1) * smallLineHeight);
          needsAnotherPass = true;
          while (needsAnotherPass) {
            needsAnotherPass = false;
            var leftBoundOfText = xValue - (textMetrics.width / 2);
            var rightBoundOfText = xValue + (textMetrics.width / 2);
            while (leftBoundOfText < (maximumXValueForRightBoundOfIdleMphTextByLine[idleMphTextVerticalOffsetLines] || 0)) {
              idleMphTextVerticalOffsetLines++;
              yValue = yValueForIdleRpm - ((idleMphTextVerticalOffsetLines - 1) * smallLineHeight);
              var rpmAtYValue = this.getRpmForYValue(yValue, topOfRpmScale, bottomMargin, topMargin, this.canvas.height);
              console.log("gear " + gearIndex + ": rpmAtYValue: " + rpmAtYValue);
              var mphAtRpm = gear.getMphForRpm(rpmAtYValue);
              xValue = this.getXValueForMph(mphAtRpm, topOfMphScale, leftMargin, rightMargin, this.canvas.width);
              needsAnotherPass = true;
            }
          }

          maximumXValueForRightBoundOfIdleMphTextByLine[idleMphTextVerticalOffsetLines] = rightBoundOfText + textPadding;
        }

        this.fillTextWithShadow(text, xValue, yValue);

        this.canvasContext.font = 'bold 14px sans-serif';
        // for gears 2 and up:
        if (gearIndex > 0) {
          // draw thin line from 0mph to the point at which we shift into this gear
          this.canvasContext.globalCompositeOperation = 'destination-over'; // draw lines behind text
          this.canvasContext.beginPath();
          this.canvasContext.lineWidth = 1;
          this.canvasContext.strokeStyle = colors[gearIndex % colors.length];
          this.canvasContext.moveTo(xValueForMphAtIdleInThisGear, yValueForIdleRpm);
          this.canvasContext.lineTo(xValueForLastMph, yValueForRpmAtLastMph);
          this.canvasContext.stroke();
          this.canvasContext.closePath();

          if (!previousGearWasSpeedLimited) {
            // label the RPM at the point where we land in this gear
            this.canvasContext.globalCompositeOperation = 'source-over'; // draw text on top of lines
            text = Math.round(rpmAtBeginningOfGear) + ' rpm';
            this.fillTextWithShadow(text, xValueForLastMph, yValueForRpmAtLastMph + lineHeight);
          }
        }

        this.canvasContext.globalCompositeOperation = 'destination-over'; // draw lines behind text
        this.canvasContext.beginPath();
        this.canvasContext.lineWidth = 3;
        this.canvasContext.strokeStyle = colors[gearIndex % colors.length];
        this.canvasContext.moveTo(xValueForLastMph, yValueForRpmAtLastMph);
        this.canvasContext.lineTo(xValueForMphAtRedlineInThisGear, yCoordinateAtMaxRpmInThisGear);
        this.canvasContext.stroke();
        this.canvasContext.closePath();

        this.canvasContext.globalCompositeOperation = 'source-over'; // draw text on top of lines

        if (speedLimited) {
          text = Math.round(mphAtRedlineInThisGear) + ' mph';
          this.fillTextWithShadow(text, xValueForMphAtRedlineInThisGear, yCoordinateAtMaxRpmInThisGear - lineHeight - descenderHeight);

          text = Math.round(rpmAtMaxSpeedInThisGear) + ' rpm';
          this.fillTextWithShadow(text, xValueForMphAtRedlineInThisGear, yCoordinateAtMaxRpmInThisGear - descenderHeight);
        } else {
          text = Math.round(mphAtRedlineInThisGear) + ' mph';
          this.fillTextWithShadow(text, xValueForMphAtRedlineInThisGear, yCoordinateAtMaxRpmInThisGear - descenderHeight);

        }

        lastMph = mphAtRedlineInThisGear;
        previousGearWasSpeedLimited = speedLimited;
      });

      this.canvasContext.globalCompositeOperation = 'source-over'; // draw text on top of lines
      var avoidRpmYCoordinate: number = 0;
      var yCoordinateOffset: number = 0;
      if (preferences.avoidRpmEnabled) {
        avoidRpmYCoordinate = this.getYValueForRpm(preferences.avoidRpm, topOfRpmScale, bottomMargin, topMargin, this.canvas.height);
        yCoordinateOffset = originY - this.getYValueForRpm(100, topOfRpmScale, bottomMargin, topMargin, this.canvas.height);
        let xCoordinateAtTopSpeed = this.getXValueForMph(topOfMphScale, topOfMphScale, leftMargin, rightMargin, this.canvas.width);
        this.canvasContext.beginPath();
        this.canvasContext.globalAlpha = 0.2;
        this.canvasContext.fillStyle = '#ff0000';
        this.canvasContext.fillRect(originX, avoidRpmYCoordinate - yCoordinateOffset, xCoordinateAtTopSpeed - originX, yCoordinateOffset * 2);
        this.canvasContext.globalAlpha = 1.0;

        let xCenter = (xCoordinateAtTopSpeed - originX) / 2 + originX;
        this.canvasContext.font = Math.round(yCoordinateOffset * 1.5) + 'px sans-serif';
        text = 'Avoid ' + preferences.avoidRpm + ' rpm';

        this.canvasContext.fillText(text, xCenter, avoidRpmYCoordinate + (yCoordinateOffset * 0.55));
        this.canvasContext.closePath();
      }

      if (preferences.topGearCruiseEnabled) {
        var topGearCruiseXCoordinate = this.getXValueForMph(preferences.topGearCruiseMph, topOfMphScale, leftMargin, rightMargin, this.canvas.width);
        var topGearCruiseYCoordinate = this.getYValueForRpm(this.result.topGearCruiseRpm, topOfRpmScale, bottomMargin, topMargin, this.canvas.height);

        this.canvasContext.beginPath();
        this.canvasContext.fillStyle = '#00cc00';
        this.canvasContext.arc(topGearCruiseXCoordinate, topGearCruiseYCoordinate, 5, 0, Math.PI * 2, false);
        this.canvasContext.fill();

        this.canvasContext.font = 'bold 14px sans-serif';
        text = preferences.topGearCruiseMph + 'mph @ ' + Math.round(this.result.topGearCruiseRpm) + 'rpm';

        let yPositionDifference = topGearCruiseYCoordinate - avoidRpmYCoordinate + yCoordinateOffset;
        if (yPositionDifference < yCoordinateOffset && yPositionDifference > 0 - (yCoordinateOffset + lineHeight)) {
          // move it to avoid overlap with the avoid RPM band
          topGearCruiseYCoordinate -= lineHeight * 2;
        }

        this.fillTextWithShadow(text, topGearCruiseXCoordinate, topGearCruiseYCoordinate + lineHeight + 5);
        this.canvasContext.closePath();
      }

      this.canvasIsUpToDate = true;
      this.consoleLog('canvas updated');
    } else {
      this.consoleLog('canvas not yet provided');
    }
  }

  fillTextWithShadow(text, x, y) {
    this.canvasContext.strokeStyle = 'white';
    this.canvasContext.lineWidth = 3;
    this.canvasContext.strokeText(text, x, y);
    this.canvasContext.fillText(text, x, y);

    // this.canvasContext.shadowBlur = 2;
    // this.canvasContext.shadowOffsetX = 0;
    // this.canvasContext.shadowOffsetY = 0;
    // this.canvasContext.shadowColor = "white";
    // this.canvasContext.fillText(text, x, y);
    // this.canvasContext.shadowBlur = 0;
    // this.canvasContext.shadowOffsetX = 0;
    // this.canvasContext.shadowOffsetY = 0;
  }

  getYValueForRpm(rpm: number, topOfRpmScale: number, bottomMargin: number, topMargin: number, canvasHeight: number): number {
    let chartableHeightInPixels = canvasHeight - topMargin - bottomMargin;
    return canvasHeight - bottomMargin - (rpm / (topOfRpmScale / chartableHeightInPixels));
  }

  getRpmForYValue(yValue: number, topOfRpmScale: number, bottomMargin: number, topMargin: number, canvasHeight: number): number {
    let chartableHeightInPixels = canvasHeight - topMargin - bottomMargin;
    return (canvasHeight - bottomMargin - yValue) * (topOfRpmScale / chartableHeightInPixels);
  }

  getXValueForMph(mph: number, topOfMphScale: number, leftMargin: number, rightMargin: number, canvasWidth: number): number {
    let chartableWidthInPixels = canvasWidth - rightMargin - leftMargin;
    return leftMargin + (mph / (topOfMphScale / chartableWidthInPixels));
  }

  consoleLog(text: string) {
    //console.log(this.uniqueId + ': ' + text);
  }

  clear() {
    this.consoleLog('clear');
    this.canvasContext.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }

  expandInputs(): void {
    this.showInputDetails = true;
    this.component.updateSetsInLocalStorage();
  }

  collapseInputs(): void {
    this.showInputDetails = false;
    this.component.updateSetsInLocalStorage();
  }

  static fromPreset(component: SpeedAndGearingComponent, preset: string, preferences: SpeedAndGearingPreferences): SpeedAndGearingSet {
    return new SpeedAndGearingSet(component, preset, CarSpec.copy(CarSpec.presets[preset]), preferences);
  }

  static copy(from: SpeedAndGearingSet, preferences: SpeedAndGearingPreferences): SpeedAndGearingSet {
    let speedAndGearingSet = new SpeedAndGearingSet(from.component, from.preset, CarSpec.copy(from.carSpec), preferences);
    speedAndGearingSet.showInputDetails = from.showInputDetails;
    return speedAndGearingSet;
  }

  onChange(preferences: SpeedAndGearingPreferences): void {
    this.carSpec.recompute();
    this.preset = '';
    this.calculate(preferences);
  }

  removeGear(preferences: SpeedAndGearingPreferences, gearNumber: number): void {
    if (this.carSpec.gearRatios.length >= 2) {
      this.preset = '';
      this.carSpec.gearRatios.splice(gearNumber, 1);
      this.calculate(preferences);
    }
  }

  addGear(preferences: SpeedAndGearingPreferences): void {
    this.preset = '';
    let lastGear = this.carSpec.topGear();
    let newRatio = lastGear.gearRatio / (lastGear.ratioSpreadFromPreviousGear == 0 ? 1.3 : lastGear.ratioSpreadFromPreviousGear);
    let roundedNewRatio = Math.round(newRatio * 100) / 100;
    this.carSpec.gearRatios.push(roundedNewRatio);
    this.calculate(preferences);
  }

  removeGearReduction(preferences: SpeedAndGearingPreferences, gearNumber: number): void {
    if (this.carSpec.finalDriveRatios.length >= 2) {
      this.preset = '';
      this.carSpec.finalDriveRatios.splice(gearNumber, 1);
      this.carSpec.recompute();
      this.calculate(preferences);
    }
  }

  addGearReduction(preferences: SpeedAndGearingPreferences): void {
    this.preset = '';
    let newRatio = 2;
    let roundedNewRatio = Math.round(newRatio * 100) / 100;
    this.carSpec.finalDriveRatios.push(roundedNewRatio);
    this.carSpec.recompute();
    this.calculate(preferences);
  }

  applyPreset(preferences: SpeedAndGearingPreferences): void {
    if (this.preset !== '') {
      this.carSpec = CarSpec.copy(CarSpec.presets[this.preset]);
      this.calculate(preferences);
    }
  }

  calculate(preferences: SpeedAndGearingPreferences) {
    this.canvasIsUpToDate = false;
    this.carSpec.recompute();
    this.result = SpeedAndGearingComponent.calculateOutput(this.carSpec, preferences);
    this.updateCanvasIfNeeded(preferences);
    this.component.updateSetsInLocalStorage();
  }

}
