import {circumferenceFromDiameter, inchesToMillimeters, millimetersToInches} from "../conversions";

export class SpeedRating {
  static all = [
    new SpeedRating("L", 75),
    new SpeedRating("M", 81),
    new SpeedRating("N", 87),
    new SpeedRating("Q", 99),
    new SpeedRating("R", 106),
    new SpeedRating("S", 112),
    new SpeedRating("T", 118),
    new SpeedRating("U", 124),
    new SpeedRating("H", 130),
    new SpeedRating("V", 149),
    new SpeedRating("Z", 149, true),
    new SpeedRating("W", 168),
    new SpeedRating("Y", 186),
    new SpeedRating("(Y)", 186, true)
  ];

  static buildLookupTable() {
    for (let speedRating of SpeedRating.all) {
      SpeedRating.allBySymbol[speedRating.symbol] = speedRating;
    }
  }

  static allBySymbol = {};

  static fromSymbol(symbol: string) {
    return SpeedRating.allBySymbol[symbol];
  }

  symbol: string;
  maxMph: number;
  plus: boolean;

  constructor(symbol: string, maxMph: number, plus: boolean = false) {
    this.symbol = symbol;
    this.maxMph = maxMph;
    this.plus = plus;
  }

  isOverSpeed(mph: number) {
    if (mph > this.maxMph) {
      if (this.plus) {
        return "maybe";
      } else {
        return "true";
      }
    }
    return "false";
  }
}

SpeedRating.buildLookupTable();

export class TireSpec {

  specType: string;
  specParameters: number[];
  wheelDiameterInches: number;
  speedRating: SpeedRating;
  sectionWidthInches: number;
  sectionWidthMillimeters: number;
  tireDiameterInches: number;
  tireDiameterMillimeters: number;
  profileHeightInches: number;
  profileHeightMillimeters: number;
  profileHeightPercentage: number;
  tireCircumferenceInches: number;
  tireCircumferenceMillimeters: number;

  recompute() {
    switch (this.specType) {
      case 'passengerCarStandard':
        this.specParameters[0] = this.sectionWidthMillimeters;
        this.specParameters[1] = this.profileHeightPercentage;

        this.sectionWidthInches = millimetersToInches(this.sectionWidthMillimeters);

        this.profileHeightInches = this.sectionWidthMillimeters * (this.profileHeightPercentage/100) / 25.4;
        this.profileHeightMillimeters = inchesToMillimeters(this.profileHeightInches);

        this.tireDiameterInches = (2 * this.profileHeightInches) + this.wheelDiameterInches;
        this.tireDiameterMillimeters = inchesToMillimeters(this.tireDiameterInches);

        this.tireCircumferenceInches = circumferenceFromDiameter(this.tireDiameterInches);
        this.tireCircumferenceMillimeters = inchesToMillimeters(this.tireCircumferenceInches)

        break;
      case 'inchMeasurements':
        this.specParameters[0] = this.sectionWidthInches;
        this.specParameters[1] = this.tireDiameterInches;

        this.sectionWidthMillimeters = inchesToMillimeters(this.sectionWidthInches);
        this.tireDiameterMillimeters = inchesToMillimeters(this.tireDiameterInches);

        this.profileHeightInches = (this.tireDiameterInches - this.wheelDiameterInches) / 2;
        this.profileHeightMillimeters = inchesToMillimeters(this.profileHeightInches);
        this.profileHeightPercentage = this.profileHeightInches / this.sectionWidthInches;

        this.tireCircumferenceInches = circumferenceFromDiameter(this.tireDiameterInches);
        this.tireCircumferenceMillimeters = inchesToMillimeters(this.tireCircumferenceInches);

        break;
      case 'metricMeasurements':
        this.specParameters[0] = this.sectionWidthMillimeters;
        this.specParameters[1] = this.tireDiameterMillimeters;

        this.sectionWidthInches = millimetersToInches(this.sectionWidthMillimeters);
        this.tireDiameterInches = millimetersToInches(this.tireDiameterMillimeters);

        this.profileHeightInches = (this.tireDiameterInches - this.wheelDiameterInches) / 2;
        this.profileHeightMillimeters = inchesToMillimeters(this.profileHeightInches);
        this.profileHeightPercentage = this.profileHeightInches / this.sectionWidthInches;

        this.tireCircumferenceInches = circumferenceFromDiameter(this.tireDiameterInches);
        this.tireCircumferenceMillimeters = inchesToMillimeters(this.tireCircumferenceInches);

        break;
      default:
        throw "Unrecognized tire spec type " + this.specType;
    }
  };

  private constructor(specType: string, specParameters: number[], wheelDiameterInches: number, speedRatingSymbol: string) {
    this.specType = specType;
    this.specParameters = [...specParameters];
    this.wheelDiameterInches = wheelDiameterInches;
    this.speedRatingSymbol = speedRatingSymbol;

  }

  static fromPassengerCarStandard(sectionWidthMillimeters: number, profileHeightPercentage: number, wheelDiameterInches: number, speedRatingSymbol: string): TireSpec {
    let tireSpec = new TireSpec('passengerCarStandard', [sectionWidthMillimeters, profileHeightPercentage], wheelDiameterInches, speedRatingSymbol);
    tireSpec.sectionWidthMillimeters = sectionWidthMillimeters;
    tireSpec.profileHeightPercentage = profileHeightPercentage;
    tireSpec.recompute();
    return tireSpec;
  }

  static fromMetricMeasurements(sectionWidthMillimeters: number, tireDiameterMillimeters: number, wheelDiameterInches: number, speedRatingSymbol: string): TireSpec {
    let tireSpec = new TireSpec('metricMeasurements', [sectionWidthMillimeters, tireDiameterMillimeters], wheelDiameterInches, speedRatingSymbol);
    tireSpec.sectionWidthMillimeters = sectionWidthMillimeters;
    tireSpec.tireDiameterMillimeters = tireDiameterMillimeters;
    tireSpec.recompute();
    return tireSpec;
  }

  static fromInchMeasurements(tireDiameterInches: number, sectionWidthInches: number, wheelDiameterInches: number, speedRatingSymbol: string): TireSpec {
    let tireSpec = new TireSpec('inchMeasurements', [tireDiameterInches, sectionWidthInches], wheelDiameterInches, speedRatingSymbol);
    tireSpec.sectionWidthInches = sectionWidthInches;
    tireSpec.tireDiameterInches = tireDiameterInches;
    tireSpec.recompute();
    return tireSpec;
  }

  set speedRatingSymbol(symbol: string) {
    this.speedRating = SpeedRating.fromSymbol(symbol);
  }

  get speedRatingSymbol() {
    return this.speedRating.symbol;
  }

  static copy(from: TireSpec) {
    let copy = new TireSpec(from.specType, from.specParameters, from.wheelDiameterInches, from.speedRating.symbol);
    switch (from.specType) {
      case 'passengerCarStandard':
        copy.sectionWidthMillimeters = from.specParameters[0];
        copy.profileHeightPercentage = from.specParameters[1];

        break;
      case 'inchMeasurements':
        copy.sectionWidthInches = from.specParameters[0];
        copy.tireDiameterInches = from.specParameters[1];

        break;
      case 'metricMeasurements':
        copy.sectionWidthMillimeters = from.specParameters[0];
        copy.tireDiameterMillimeters = from.specParameters[1];

        break;
      default:
        throw "Unrecognized tire spec type " + from.specType;
    }
    copy.recompute();
    return copy;
  }
}

export class GearSpec {
  gearNumber: number;
  gearRatio: number;
  tireRevsPerEngineRev: number;
  mphPerRpm: number;
  mphAtMaxRpm: number;
  ratioSpreadFromPreviousGear: number;
  shiftFromPreviousGearRpm: number;

  constructor(previousGear: GearSpec, gearRatio: number, carSpec: CarSpec) {
    this.gearNumber = previousGear === undefined ? 1 : previousGear.gearNumber + 1;
    this.gearRatio = gearRatio;
    this.tireRevsPerEngineRev = 1 / (gearRatio * carSpec.combinedFinalDriveRatio);
    this.mphPerRpm = this.tireRevsPerEngineRev * carSpec.tireSpec.tireCircumferenceInches * 60 / (5280 * 12);
    this.mphAtMaxRpm = this.getMphForRpm(carSpec.maximumRpm);
    this.ratioSpreadFromPreviousGear = previousGear === undefined ? 0 : previousGear.gearRatio / gearRatio;
    this.shiftFromPreviousGearRpm = previousGear === undefined ? 0 : this.getRpmForMph(previousGear.mphAtMaxRpm);
  }

  getMphForRpm(rpm: number) {
    return this.mphPerRpm * rpm;
  }

  getRpmForMph(desiredMph: number) {
    return desiredMph / this.mphPerRpm;
  }

  print() {
    console.log(this.gearNumber + ": " + this.gearRatio + ":1 -- " + this.tireRevsPerEngineRev + " tire revs per engine rev -- " + this.mphPerRpm + " per RPM");
  }
}

export class CarSpec {
  static presets = {
    "LS1 T56 Camaro Z28 / Trans Am ": new CarSpec(TireSpec.fromPassengerCarStandard(245, 50, 16, "W"), [2.66, 1.78, 1.30, 1.0, 0.74, 0.50], [3.42], 700, 6200),
    "LS1 T56 Camaro SS / Trans Am WS6": new CarSpec(TireSpec.fromPassengerCarStandard(275, 40, 17, "Y"), [2.66, 1.78, 1.30, 1.0, 0.74, 0.50], [3.42], 700, 6200),
    "LS1 GTO M6": new CarSpec(TireSpec.fromPassengerCarStandard(225, 50, 17, "Y"), [2.97, 2.07, 1.43, 1.0, 0.87, 0.57], [3.46], 700, 6200),
    "C5 Z06": new CarSpec(TireSpec.fromPassengerCarStandard(295, 35, 18, "Y"), [2.97, 2.07, 1.43, 1.0, 0.84, 0.56], [3.42], 700, 6500),
    "997 911 Carrera": new CarSpec(TireSpec.fromPassengerCarStandard(265, 40, 18, "Y"), [3.91, 2.32, 1.64, 1.28, 1.08, 0.88], [3.44], 700, 7300),
    "2016 Camaro SS M6": new CarSpec(TireSpec.fromPassengerCarStandard(275, 35, 20, "Y"), [2.66, 1.78, 1.30, 1.0, 0.74, 0.50], [3.73], 700, 6600),
    "2016 Camaro SS A8": new CarSpec(TireSpec.fromPassengerCarStandard(275, 35, 20, "Y"), [4.56, 2.97, 2.08, 1.69, 1.27, 1.0, 0.85, 0.65], [2.77], 700, 6600),
    "2017 ZL1 Auto": new CarSpec(TireSpec.fromPassengerCarStandard(305, 30, 20, "(Y)"), [4.70, 2.99, 2.15, 1.8, 1.52, 1.28, 1.0, 0.85, 0.69, 0.64], [2.85], 700, 6600),
    "2018 ZL1 1LE": new CarSpec(TireSpec.fromPassengerCarStandard(325, 30, 19, "(Y)"), [2.66, 1.78, 1.30, 1.0, 0.74, 0.68], [3.73], 700, 6600),
    "AP1 S2000": new CarSpec(TireSpec.fromPassengerCarStandard(225, 50, 16, "W"), [3.133, 2.045, 1.481, 1.161, 0.971, 0.811], [4.1, 1.16], 800, 9000),
    "AP2 S2000": new CarSpec(TireSpec.fromPassengerCarStandard(245, 40, 17, "W"), [3.133, 2.045, 1.481, 1.161, 0.942, 0.763], [4.1, 1.208], 800, 8200),
    "AP2 S2000 CR": new CarSpec(TireSpec.fromPassengerCarStandard(255, 40, 17, "W"), [3.133, 2.045, 1.481, 1.161, 0.942, 0.763], [4.1, 1.208], 800, 8200),
    "Neon SRT4": new CarSpec(TireSpec.fromPassengerCarStandard(205, 50, 17, "(Y)"), [3.65, 2.05, 1.37, 0.97, 0.76], [3.53], 700, 6500),
    "S550 Mustang MT82": new CarSpec(TireSpec.fromPassengerCarStandard(305, 30, 19, "Y"), [3.657, 2.430, 1.686, 1.315, 1, 0.651], [3.55], 700, 8000),
    "Gordon Murray Automotive T.50": new CarSpec(TireSpec.fromPassengerCarStandard(295, 30, 20, "(Y)"), [2.833, 2.095, 1.577, 1.226, 0.971, 0.744], [3.176, 1.688], 700, 12400),
    "Gordon Murray Automotive T.50 with optional sixth gear": new CarSpec(TireSpec.fromPassengerCarStandard(295, 30, 20, "(Y)"), [2.833, 2.095, 1.577, 1.226, 0.971, 0.595], [3.176, 1.688], 700, 12400)
  };

  tireSpec: TireSpec;
  gearRatios: number[];
  gears: GearSpec[];
  finalDriveRatios: number[];
  combinedFinalDriveRatio: number;
  minimumRpm: number;
  maximumRpm: number;

  recompute() {
    this.combinedFinalDriveRatio = this.finalDriveRatios.reduce( (a,b) => a * b );
    this.tireSpec.recompute();
    this.computeGears();
  };

  private computeGears() {
    this.gears = [];
    var gearSpec: GearSpec = undefined;
    for (var gearIndex = 0; gearIndex < this.gearRatios.length; gearIndex++) {
      var gearRatio: number = this.gearRatios[gearIndex];
      gearSpec = new GearSpec(gearSpec, gearRatio, this);
      // gearSpec.print();
      this.gears.push(gearSpec);
    }
  }

  constructor(tireSpec: TireSpec, gearRatios: number[], finalDriveRatios: number[], minimumRpm: number, maximumRpm: number) {
    this.tireSpec = tireSpec;
    this.gearRatios = gearRatios;
    this.finalDriveRatios = finalDriveRatios;
    this.minimumRpm = minimumRpm;
    this.maximumRpm = maximumRpm;
    this.recompute();
  }

  static copy(from: CarSpec) {
    return new CarSpec(TireSpec.copy(from.tireSpec), [...from.gearRatios], [...from.finalDriveRatios], from.minimumRpm, from.maximumRpm);
  }

  topSpeed() {
    if (this.tireSpec.speedRating.plus) {
      return this.topGear().getMphForRpm(this.maximumRpm);
    } else {
      return Math.min(this.topGear().getMphForRpm(this.maximumRpm), this.tireSpec.speedRating.maxMph);
    }
  }

  topGear(): GearSpec {
    return this.gears[this.gears.length - 1];
  }
}

