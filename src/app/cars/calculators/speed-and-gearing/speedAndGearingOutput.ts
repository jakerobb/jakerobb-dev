import {CarSpec, GearSpec} from "./speedAndGearingInput";

export class SpeedAndGearingOutput {
  carSpec: CarSpec;
  mphTable: MphRow[];
  topGearCruiseRpm: number;
  roundedTopGearCruiseRpm: number;
}

export class MphRow {
  rpm: number;
  mphByGear: MphEntry[];
  avoid: boolean;

  constructor(rpm: number, mphByGear: MphEntry[], avoid: boolean) {
    this.rpm = rpm;
    this.mphByGear = mphByGear;
    this.avoid = avoid;
  }

  print() {
    console.log(this.rpm + ": " +
      this.mphByGear[0] + ", " +
      this.mphByGear[1] + ", " +
      this.mphByGear[2] + ", " +
      this.mphByGear[3] + ", " +
      this.mphByGear[4] + ", " +
      this.mphByGear[5])
  }
}

export class MphEntry {
  gear: GearSpec;
  mph: number;
  overSpeed: string;

  constructor(gear: GearSpec, mph: number, overSpeed: string) {
    this.gear = gear;
    this.mph = mph;
    this.overSpeed = overSpeed;
  }
}

