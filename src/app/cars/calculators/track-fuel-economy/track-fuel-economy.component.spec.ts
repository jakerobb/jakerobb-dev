import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackFuelEconomyComponent } from './track-fuel-economy.component';

describe('TrackFuelEconomyComponent', () => {
  let component: TrackFuelEconomyComponent;
  let fixture: ComponentFixture<TrackFuelEconomyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackFuelEconomyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackFuelEconomyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
