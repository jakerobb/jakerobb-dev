import { Component, OnInit } from '@angular/core';
import { TrackFuelEconomyInput } from './trackFuelEconomyInput';
import { TrackFuelEconomyResult } from './trackFuelEconomyResult';

@Component({
  selector: 'app-track-fuel-economy',
  templateUrl: './track-fuel-economy.component.html',
  styleUrls: ['./track-fuel-economy.component.scss']
})
export class TrackFuelEconomyComponent implements OnInit {

  input: TrackFuelEconomyInput = {
    lapLength: 2.0,
    lapDurationMinutes: 1,
    lapDurationSeconds: 35,
    sessionDurationHours: 0,
    sessionDurationMinutes: 20,
    sessionDurationSeconds: 0,
    sessionsPerEvent: 6,
    averageFuelEconomy: 4.5,
    tankSize: 16.8,
    fuelCost: 3.499
  };

  result: TrackFuelEconomyResult = {
    totalLapDurationSeconds: 0,
    totalSessionDurationSeconds: 0,
    lapsPerSession: 0,
    milesPerSession: 0,
    sessionConsumption: 0,
    sessionsPerTank: 0,
    sessionGallonsRemaining: 0,
    sessionCost: 0,
    totalFuelUsed: 0,
    totalFuelCost: 0,
    additionalFuelNeeded: 0
  };

  constructor() { }

  calculate = function(input, result) {
    result.totalLapDurationSeconds = input.lapDurationMinutes * 60 + input.lapDurationSeconds;
    result.totalSessionDurationSeconds = input.sessionDurationHours * 3600 + input.sessionDurationMinutes * 60 + input.sessionDurationSeconds;
    result.lapsPerSession = result.totalSessionDurationSeconds / result.totalLapDurationSeconds;
    result.milesPerSession = result.lapsPerSession * input.lapLength;
    result.sessionConsumption = result.milesPerSession / input.averageFuelEconomy;
    result.sessionsPerTank = Math.floor(input.tankSize / result.sessionConsumption);
    result.sessionGallonsRemaining = ((input.tankSize / result.sessionConsumption) - result.sessionsPerTank) * result.sessionConsumption;
    result.sessionCost = result.sessionConsumption * input.fuelCost;
    result.totalFuelUsed = result.sessionConsumption * input.sessionsPerEvent;
    result.totalFuelCost = result.totalFuelUsed * input.fuelCost;
    result.additionalFuelNeeded = result.totalFuelUsed - input.tankSize;
  };

  ngOnInit() {
    this.calculate(this.input, this.result);
  }

  onChange() {
    this.calculate(this.input, this.result);
  }

}
