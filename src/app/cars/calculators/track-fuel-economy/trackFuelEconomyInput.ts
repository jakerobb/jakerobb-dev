export class TrackFuelEconomyInput {
  lapLength: number;
  lapDurationMinutes: number;
  lapDurationSeconds: number;
  sessionDurationHours: number;
  sessionDurationMinutes: number;
  sessionDurationSeconds: number;
  sessionsPerEvent: number;
  averageFuelEconomy: number;
  tankSize: number;
  fuelCost: number;
}
