export class TrackFuelEconomyResult {
  totalLapDurationSeconds: number;
  totalSessionDurationSeconds: number;
  lapsPerSession: number;
  milesPerSession: number;
  sessionConsumption: number;
  sessionsPerTank: number;
  sessionGallonsRemaining: number;
  sessionCost: number;
  totalFuelUsed: number;
  totalFuelCost: number;
  additionalFuelNeeded: number;
}
