import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WheelAndTireSizeComponent } from './wheel-and-tire-size.component';

describe('WheelAndTireSizeComponent', () => {
  let component: WheelAndTireSizeComponent;
  let fixture: ComponentFixture<WheelAndTireSizeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WheelAndTireSizeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WheelAndTireSizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
