import {Component, ElementRef, OnInit, QueryList, ViewChildren} from '@angular/core';
import {animate, animateChild, query, stagger, style, transition, trigger} from "@angular/animations";
import {v4 as uuid} from 'uuid';
import {inchesToMillimeters, millimetersToInches} from "../conversions";

@Component({
  selector: 'app-wheel-and-tire-size',
  templateUrl: './wheel-and-tire-size.component.html',
  styleUrls: ['./wheel-and-tire-size.component.scss'],

  animations: [
    trigger('list', [
      transition(':enter', [
        query('@inputs', stagger(50, animateChild())),
        query('@outputs', stagger(0, animateChild())),
        query('@canvasses', stagger(0, animateChild()))
      ]),
    ]),
    trigger('inputs', [
      transition(':enter', [
        style({transform: 'scale(0, 1)', opacity: 1, 'transform-origin': 'left'}),
        animate('200ms ease-in', style({transform: 'scale(1, 1)', opacity: 1, 'transform-origin': 'left'}))
      ]),
      transition(':leave', [
        animate('200ms ease-in', style({transform: 'scale(0, 1)', opacity: 1, 'transform-origin': 'left'}))
      ])
    ]),
    trigger('outputs', [
      transition(':enter', [
        style({transform: 'scale(0, 1)', opacity: 1, 'transform-origin': 'left'}),
        animate('200ms ease-in', style({transform: 'scale(1, 1)', opacity: 1, 'transform-origin': 'left'}))
      ]),
      transition(':leave', [
        animate('200ms ease-in', style({transform: 'scale(0, 1)', opacity: 1, 'transform-origin': 'left'}))
      ])
    ]),
    trigger('canvasses', [
      transition(':enter', [
        style({transform: 'scale(0, 1)', opacity: 1, 'transform-origin': 'left'}),
        animate('200ms ease-in', style({transform: 'scale(1, 1)', opacity: 1, 'transform-origin': 'left'}))
      ]),
      transition(':leave', [
        animate('200ms ease-in', style({transform: 'scale(0, 1)', opacity: 1, 'transform-origin': 'left'}))
      ])
    ])
  ]
})
export class WheelAndTireSizeComponent implements OnInit {
  sizeRatioExampleSpeeds: number[] = [25, 50, 75, 100];
  sets: Set[];
  scaleFactor: number = (window.devicePixelRatio || 1) * 100;

  @ViewChildren('wheelAndTireCanvas') canvasContexts:QueryList<ElementRef<HTMLCanvasElement>>;
  constructor(private elementRef: ElementRef) {
  }

  ngOnInit() {
    this.sets = [];
    this.sets[0] = new Set(this, new Wheel(this, 17, 9, 56), new Tire(this,  275, 40, 17), new Brake(13, 35, 35, 5), this.scaleFactor / 100);
    this.changeItemCount();
    this.adjustCanvasSizesAndRedrawIfNeeded();
  }

  ngAfterViewInit(): void {
    var i = 0;
    this.canvasContexts.changes.subscribe(newList => {
      var j = 0;
      newList.forEach(canvasElement => this.sets[j++].provideCanvas(canvasElement))
    });
    this.canvasContexts.forEach(canvasElement => this.sets[i++].provideCanvas(canvasElement));
    this.adjustCanvasSizesAndRedrawIfNeeded();
  }

  recomputeAll() {
    this.sets.forEach(function (set) {
      set.recompute();
    });
  }

  onChangeScaleFactor() {
    let parentComponent = this;
    this.sets.forEach(function (set) {
      set.setScaleFactor(parentComponent.scaleFactor / 100);
      set.recompute();
    });
  }

  uniqueId(index: any, item: any) {
    return item.uniqueId;
  }

  addItem() {
    this.sets.push(Set.copy(this.sets[this.sets.length - 1]));
    this.changeItemCount();
  }

  changeItemCount() {
    this.elementRef.nativeElement.style.setProperty('--setCount', this.sets.length);
    this.adjustCanvasSizesAndRedrawIfNeeded();
  }

  swapItem(setIndex) {
    let tempSet = this.sets[setIndex];
    this.sets[setIndex] = this.sets[setIndex - 1];
    this.sets[setIndex - 1] = tempSet;
    this.recomputeAll();
  }

  cloneItem(setIndex) {
    this.sets.splice(setIndex + 1, 0, Set.copy(this.sets[setIndex]));
    this.changeItemCount();
  }

  removeItem(setIndex) {
    this.sets.splice(setIndex, 1);
    this.changeItemCount();
  }

  adjustCanvasSizesAndRedrawIfNeeded() {
    // console.log('adjustCanvasSizesAndRedrawIfNeeded');
    let parentComponent = this;
    var largestTireWidthMillimeters = 0;
    var largestTireDiameterInches = 0;
    this.sets.forEach(function (set) {
      largestTireWidthMillimeters = Math.max(largestTireWidthMillimeters, set.tire.sectionWidthMillimeters);
      largestTireDiameterInches = Math.max(largestTireDiameterInches, set.tire.tireDiameterInches);
    });
    this.sets.forEach(function (set) {
      set.canvasWidth = set.millimetersToPixels(largestTireWidthMillimeters) + Set.canvasMargin * 2;
      set.canvasHeight = set.inchesToPixels(largestTireDiameterInches) + Set.canvasMargin * 2;
      set.onChangeCanvasSize();
    });
  }

}

export class Set {
  // tunable parameters -- should not be user facing
  static canvasMargin = 10;
  static bezierCurveInfluenceFactor = 2;

  component: WheelAndTireSizeComponent;
  wheel: Wheel;
  tire: Tire;
  brake: Brake;
  uniqueId: string;
  canvasWidth: number;
  canvasHeight: number;
  canvas: HTMLCanvasElement;
  canvasContext: CanvasRenderingContext2D;
  canvasIsUpToDate: boolean = false;

  private scaleFactor;
  // "constant" measurements that only update based on scaleFactor
  oneInch: number;
  twoInches: number;
  threeInches: number;
  quarterInch: number;
  halfInch: number;
  threeQuartersOfAnInch: number;
  oneAndAHalfInches: number;
  oneMillimeter: number;
  twoMillimeters: number;
  threeMillimeters: number;
  fourMillimeters: number;
  fiveMillimeters: number;

  constructor(component: WheelAndTireSizeComponent, wheel: Wheel, tire: Tire, brake: Brake, scaleFactor: number) {
    this.component = component;
    this.wheel = wheel;
    this.tire = tire;
    this.brake = brake;
    this.uniqueId = uuid();
    this.setScaleFactor(scaleFactor);
  }

  consoleLog(text: string) {
    // console.log(this.uniqueId + ': ' + text);
  }

  recompute() {
    this.tire.wheelDiameterInches = this.wheel.diameterInches;
    this.tire.recompute();
    this.wheel.recompute();
    this.component.adjustCanvasSizesAndRedrawIfNeeded();
  }

  onChange() {
    this.canvasIsUpToDate = false;
    this.recompute();
    if (this === this.component.sets[0]) {
      this.component.recomputeAll();
    }
  }

  provideCanvas(canvasElement: ElementRef) {
    this.consoleLog('provideCanvas');
    this.canvas = <HTMLCanvasElement> canvasElement.nativeElement;
    this.canvasContext = this.canvas.getContext('2d');
    this.onChangeCanvasSize();
  }

  onChangeCanvasSize() {
    this.consoleLog('onChangeCanvasSize');
    if (this.canvas) {
      this.consoleLog('Desired size is ' + this.canvasWidth + 'x' + this.canvasHeight +'.');
      this.consoleLog('Actual size is ' + this.canvas.width + 'x' + this.canvas.height +'.');
      if (this.canvas.width !== this.canvasWidth || this.canvas.height !== this.canvasHeight) {
        this.consoleLog('Adjusting size...');
        this.canvas.width = this.canvasWidth;
        this.canvas.height = this.canvasHeight;
        this.canvasContext.scale(1, 1);
        this.canvasIsUpToDate = false;
      } else {
        this.consoleLog('Already correct!');
      }
      this.redrawIfNeeded();
    } else {
      this.consoleLog('canvas not provided yet');
    }
  }

  redrawIfNeeded() {
    this.consoleLog('redrawIfNeeded');
    if (!this.canvasIsUpToDate) {
      this.draw();
    }
  }

  draw() {
    this.consoleLog('draw');
    this.clear();

    // all local variables are in pixels!!

    // potentially customizable dimensions
    let wheelHoopThickness = this.fourMillimeters;
    let wheelSpokeThickness = this.fourMillimeters;
    let wheelSpokeToMountingFaceRadius = this.oneInch;
    let dropCenterDepth = this.oneInch;
    let dropCenterWidth = this.oneInch;
    let dropCenterOuterAngleWidth = this.oneInch;
    let dropCenterInnerAngleWidth = this.oneInch;
    let tireCrossSectionThickness = this.fourMillimeters;
    let tireTreadEdgeArcRadius = this.halfInch;
    let caliperSlideThickness = this.threeQuartersOfAnInch;
    let caliperSlideClearance = this.threeMillimeters;
    let rotorHatToPadClearance = this.halfInch;
    let caliperToHatClearance = this.quarterInch;
    let unsweptOuterRotorRadius = this.threeMillimeters;
    let caliperExtentBeyondPad = this.quarterInch;
    let caliperSideThickness = this.oneInch;
    let padThickness = this.halfInch;
    let lipHeight = this.halfInch;
    let fudgeFactor = this.twoMillimeters; // for when stuff just doesn't line up by the math :/


    // dimension calculations
    let tireWidthPixels = this.millimetersToPixels(this.tire.sectionWidthMillimeters);


    // location calculations
    let topOfTire = Set.canvasMargin;
    let leftOfTire = Set.canvasMargin;
    let rightOfTire = leftOfTire + tireWidthPixels;
    let tireProfileHeight = this.inchesToPixels(this.tire.profileHeightInches);
    let bottomOfTopOfTire = topOfTire + tireProfileHeight;
    let topOfBottomOfTire = bottomOfTopOfTire + this.inchesToPixels(this.tire.wheelDiameterInches);
    let bottomOfTire = topOfBottomOfTire + tireProfileHeight;

    let bezierControlPointOffsets = tireProfileHeight / Set.bezierCurveInfluenceFactor;

    let horizontalCenter = leftOfTire + ((rightOfTire - leftOfTire) / 2);
    let verticalCenter = topOfTire + ((bottomOfTire - topOfTire) / 2);

    let wheelWidth = this.inchesToPixels(this.wheel.widthInches);
    let wheelHeight = this.inchesToPixels(this.wheel.diameterInches);

    let leftOfWheel = horizontalCenter - (wheelWidth / 2);
    let rightOfWheel = leftOfWheel + wheelWidth;

    let topOfWheel = verticalCenter - (wheelHeight / 2);
    let bottomOfWheel = topOfWheel + wheelHeight;

    let wheelMountingFace = horizontalCenter - this.millimetersToPixels(this.wheel.offsetMillimeters);
    let spokeX = wheelMountingFace - wheelSpokeToMountingFaceRadius;

    let leftOfBrakeRotorHat = wheelMountingFace + this.twoMillimeters;
    let rightOfBrakeRotorHat = leftOfBrakeRotorHat + this.millimetersToPixels(this.brake.hatThicknessMillimeters);
    let hatRadius = this.inchesToPixels(this.brake.hatDiameterInches / 2);
    let topOfBrakeRotorHat = verticalCenter - hatRadius;
    let bottomOfBrakeRotorHat = verticalCenter + hatRadius;

    let leftOfBrakeRotor = rightOfBrakeRotorHat - 1;
    let rightOfBrakeRotor = leftOfBrakeRotor + this.millimetersToPixels(this.brake.rotorThicknessMillimeters);
    let rotorRadius = this.inchesToPixels(this.brake.rotorDiameterInches / 2);
    let topOfBrakeRotor = verticalCenter - rotorRadius;
    let bottomOfBrakeRotor = verticalCenter + rotorRadius;

    let sweptRadius = topOfBrakeRotorHat - topOfBrakeRotor - rotorHatToPadClearance - caliperToHatClearance;
    let leftOfCaliper = leftOfBrakeRotor - caliperSideThickness - padThickness;
    let topOfCaliper = topOfBrakeRotor - caliperSlideThickness - caliperSlideClearance;
    let rightOfCaliper = rightOfBrakeRotor + caliperSideThickness + padThickness;
    let bottomOfCaliper = topOfBrakeRotorHat - caliperToHatClearance;
    let bottomOfCaliperSlide = topOfCaliper + caliperSlideThickness;

    let topOfPads = topOfBrakeRotor + unsweptOuterRotorRadius;
    let bottomOfPads = bottomOfCaliper - caliperExtentBeyondPad;
    let leftOfLeftPad = leftOfBrakeRotor - padThickness;
    let rightOfRightPad = rightOfBrakeRotor + padThickness;

    this.drawWheel(wheelHoopThickness, leftOfWheel, fudgeFactor, topOfWheel, lipHeight, spokeX, dropCenterOuterAngleWidth, dropCenterDepth, dropCenterWidth, dropCenterInnerAngleWidth, rightOfWheel, bottomOfWheel, wheelSpokeThickness, topOfBrakeRotorHat, wheelSpokeToMountingFaceRadius, wheelMountingFace, bottomOfBrakeRotorHat);
    this.drawTire(tireCrossSectionThickness, leftOfWheel, fudgeFactor, bottomOfTopOfTire, topOfTire, bezierControlPointOffsets, leftOfTire, tireTreadEdgeArcRadius, rightOfTire, rightOfWheel, topOfBottomOfTire, bottomOfTire);
    this.drawBrake(leftOfBrakeRotorHat, topOfBrakeRotorHat, rightOfBrakeRotorHat, bottomOfBrakeRotorHat, leftOfBrakeRotor, topOfBrakeRotor, rightOfBrakeRotor, bottomOfBrakeRotor, leftOfCaliper, topOfCaliper, leftOfLeftPad, bottomOfCaliper, rightOfRightPad, bottomOfCaliperSlide, rightOfCaliper, topOfPads, bottomOfPads);

    this.canvasIsUpToDate = true;
  }

  private drawBrake(leftOfBrakeRotorHat, topOfBrakeRotorHat, rightOfBrakeRotorHat, bottomOfBrakeRotorHat, leftOfBrakeRotor, topOfBrakeRotor, rightOfBrakeRotor, bottomOfBrakeRotor, leftOfCaliper, topOfCaliper, leftOfLeftPad, bottomOfCaliper, rightOfRightPad, bottomOfCaliperSlide, rightOfCaliper, topOfPads, bottomOfPads) {
    // draw brake rotor hat
    this.canvasContext.beginPath();
    this.canvasContext.strokeStyle = '#A9A9A9';
    this.canvasContext.fillStyle = '#A9A9A9';
    this.fillRectAbsolute(leftOfBrakeRotorHat, topOfBrakeRotorHat, rightOfBrakeRotorHat, bottomOfBrakeRotorHat);

    // draw brake rotor
    this.fillRectAbsolute(leftOfBrakeRotor, topOfBrakeRotor, rightOfBrakeRotor, bottomOfBrakeRotor);
    this.canvasContext.closePath();

    // draw brake caliper
    this.canvasContext.beginPath();
    this.canvasContext.fillStyle = '#CC0000';
    this.fillRectAbsolute(leftOfCaliper, topOfCaliper, leftOfLeftPad, bottomOfCaliper);
    this.fillRectAbsolute(leftOfLeftPad, topOfCaliper, rightOfRightPad, bottomOfCaliperSlide);
    this.fillRectAbsolute(rightOfRightPad, topOfCaliper, rightOfCaliper, bottomOfCaliper);
    this.canvasContext.closePath();

    // draw brake pads
    this.canvasContext.beginPath();
    this.canvasContext.fillStyle = '#555555';
    this.fillRectAbsolute(leftOfLeftPad, topOfPads, leftOfBrakeRotor, bottomOfPads);
    this.fillRectAbsolute(rightOfBrakeRotor, topOfPads, rightOfRightPad, bottomOfPads);
    this.canvasContext.closePath();
  }

  private drawTire(tireCrossSectionThickness, leftOfWheel, fudgeFactor, bottomOfTopOfTire, topOfTire, bezierControlPointOffsets, leftOfTire, tireTreadEdgeArcRadius, rightOfTire, rightOfWheel, topOfBottomOfTire, bottomOfTire) {
    // draw top tire cross-section
    this.canvasContext.beginPath();
    this.canvasContext.lineWidth = tireCrossSectionThickness;
    this.canvasContext.strokeStyle = '#000000';
    this.drawBezierCurve(leftOfWheel + fudgeFactor, bottomOfTopOfTire - fudgeFactor, leftOfWheel, topOfTire + bezierControlPointOffsets, leftOfTire, bottomOfTopOfTire - bezierControlPointOffsets, leftOfTire, topOfTire + tireTreadEdgeArcRadius);
    this.drawArc(leftOfTire + tireTreadEdgeArcRadius, topOfTire + tireTreadEdgeArcRadius, tireTreadEdgeArcRadius, Math.PI, Math.PI * 1.5, true);
    this.drawLine(leftOfTire + tireTreadEdgeArcRadius, topOfTire, rightOfTire - tireTreadEdgeArcRadius, topOfTire); // top
    this.drawArc(rightOfTire - tireTreadEdgeArcRadius, topOfTire + tireTreadEdgeArcRadius, tireTreadEdgeArcRadius, Math.PI * 1.5, Math.PI * 2, true);
    this.drawBezierCurve(rightOfTire, topOfTire + tireTreadEdgeArcRadius, rightOfTire, bottomOfTopOfTire - bezierControlPointOffsets, rightOfWheel, topOfTire + bezierControlPointOffsets, rightOfWheel - fudgeFactor, bottomOfTopOfTire - fudgeFactor);
    this.canvasContext.closePath();

    // draw bottom tire cross-section
    this.canvasContext.beginPath();
    this.canvasContext.lineWidth = tireCrossSectionThickness;
    this.canvasContext.strokeStyle = '#000000';
    this.drawBezierCurve(leftOfWheel + fudgeFactor, topOfBottomOfTire + fudgeFactor, leftOfWheel, bottomOfTire - bezierControlPointOffsets, leftOfTire, topOfBottomOfTire + bezierControlPointOffsets, leftOfTire, bottomOfTire - tireTreadEdgeArcRadius);
    this.drawArc(leftOfTire + tireTreadEdgeArcRadius, bottomOfTire - tireTreadEdgeArcRadius, tireTreadEdgeArcRadius, Math.PI, Math.PI / 2, false);
    this.drawLine(leftOfTire + tireTreadEdgeArcRadius, bottomOfTire, rightOfTire - tireTreadEdgeArcRadius, bottomOfTire); // top
    this.drawArc(rightOfTire - tireTreadEdgeArcRadius, bottomOfTire - tireTreadEdgeArcRadius, tireTreadEdgeArcRadius, Math.PI / 2, 0, false);
    this.drawBezierCurve(rightOfTire, bottomOfTire - tireTreadEdgeArcRadius, rightOfTire, topOfBottomOfTire + bezierControlPointOffsets, rightOfWheel, bottomOfTire - bezierControlPointOffsets, rightOfWheel - fudgeFactor, topOfBottomOfTire + fudgeFactor);
    this.canvasContext.closePath();
  }

  private drawWheel(wheelHoopThickness, leftOfWheel, fudgeFactor, topOfWheel, lipHeight, spokeX, dropCenterOuterAngleWidth, dropCenterDepth, dropCenterWidth, dropCenterInnerAngleWidth, rightOfWheel, bottomOfWheel, wheelSpokeThickness, topOfBrakeRotorHat, wheelSpokeToMountingFaceRadius, wheelMountingFace, bottomOfBrakeRotorHat) {
    this.drawTopWheelCrossSection(wheelHoopThickness, leftOfWheel, fudgeFactor, topOfWheel, lipHeight, spokeX, dropCenterOuterAngleWidth, dropCenterDepth, dropCenterWidth, dropCenterInnerAngleWidth, rightOfWheel);
    this.drawBottomWheelCrossSection(wheelHoopThickness, leftOfWheel, fudgeFactor, bottomOfWheel, lipHeight, spokeX, dropCenterOuterAngleWidth, dropCenterDepth, dropCenterWidth, dropCenterInnerAngleWidth, rightOfWheel);
    this.drawWheelCenter(wheelSpokeThickness, spokeX, topOfWheel, dropCenterDepth, topOfBrakeRotorHat, wheelSpokeToMountingFaceRadius, wheelMountingFace, fudgeFactor, bottomOfBrakeRotorHat, bottomOfWheel);
  }

  private drawWheelCenter(wheelSpokeThickness, spokeX, topOfWheel, dropCenterDepth, topOfBrakeRotorHat, wheelSpokeToMountingFaceRadius, wheelMountingFace, fudgeFactor, bottomOfBrakeRotorHat, bottomOfWheel) {
    // draw wheel center
    this.canvasContext.beginPath();
    this.canvasContext.lineWidth = wheelSpokeThickness;
    this.canvasContext.strokeStyle = '#C0C0C0';
    this.drawLine(spokeX, topOfWheel + dropCenterDepth, spokeX, topOfBrakeRotorHat - wheelSpokeToMountingFaceRadius);
    this.drawArc(wheelMountingFace, topOfBrakeRotorHat - wheelSpokeToMountingFaceRadius + fudgeFactor, wheelSpokeToMountingFaceRadius, Math.PI, Math.PI / 2, false);
    this.drawLine(wheelMountingFace, topOfBrakeRotorHat, wheelMountingFace, bottomOfBrakeRotorHat);
    this.drawArc(wheelMountingFace, bottomOfBrakeRotorHat + wheelSpokeToMountingFaceRadius - fudgeFactor, wheelSpokeToMountingFaceRadius, 1.5 * Math.PI, Math.PI, false);
    this.drawLine(spokeX, bottomOfBrakeRotorHat + wheelSpokeToMountingFaceRadius - fudgeFactor, spokeX, bottomOfWheel - dropCenterDepth);
    this.canvasContext.closePath();
  }

  private drawBottomWheelCrossSection(wheelHoopThickness, leftOfWheel, fudgeFactor, bottomOfWheel, lipHeight, spokeX, dropCenterOuterAngleWidth, dropCenterDepth, dropCenterWidth, dropCenterInnerAngleWidth, rightOfWheel) {
    // draw bottom wheel cross section
    this.canvasContext.beginPath();
    this.canvasContext.lineWidth = wheelHoopThickness;
    this.canvasContext.strokeStyle = '#C0C0C0';

    // outer lip
    this.drawLine(leftOfWheel - fudgeFactor, bottomOfWheel - fudgeFactor, leftOfWheel - fudgeFactor, bottomOfWheel + lipHeight);

    // this is a drop center, inverted.
    //   _
    // _/ \__
    //
    // it has five line segments:
    this.drawLine(leftOfWheel, bottomOfWheel, spokeX - dropCenterOuterAngleWidth, bottomOfWheel);
    this.drawLine(spokeX - dropCenterOuterAngleWidth, bottomOfWheel, spokeX, bottomOfWheel - dropCenterDepth);
    this.drawLine(spokeX, bottomOfWheel - dropCenterDepth, spokeX + dropCenterWidth, bottomOfWheel - dropCenterDepth);
    this.drawLine(spokeX + dropCenterWidth, bottomOfWheel - dropCenterDepth, spokeX + dropCenterWidth + dropCenterInnerAngleWidth, bottomOfWheel);
    this.drawLine(spokeX + dropCenterWidth + dropCenterInnerAngleWidth, bottomOfWheel, rightOfWheel, bottomOfWheel);

    // inner lip
    this.drawLine(rightOfWheel + fudgeFactor, bottomOfWheel - fudgeFactor, rightOfWheel + fudgeFactor, bottomOfWheel + lipHeight);
    this.canvasContext.closePath();
  }

  private drawTopWheelCrossSection(wheelHoopThickness, leftOfWheel, fudgeFactor, topOfWheel, lipHeight, spokeX, dropCenterOuterAngleWidth, dropCenterDepth, dropCenterWidth, dropCenterInnerAngleWidth, rightOfWheel) {
    // draw top wheel cross section
    this.canvasContext.beginPath();
    this.canvasContext.lineWidth = wheelHoopThickness;
    this.canvasContext.strokeStyle = '#C0C0C0';

    // outer lip
    this.drawLine(leftOfWheel - fudgeFactor, topOfWheel + fudgeFactor, leftOfWheel - fudgeFactor, topOfWheel - lipHeight);

    // this is a drop center.
    // _   _
    //  \_/
    //
    // it has five line segments:
    this.drawLine(leftOfWheel, topOfWheel, spokeX - dropCenterOuterAngleWidth, topOfWheel);
    this.drawLine(spokeX - dropCenterOuterAngleWidth, topOfWheel, spokeX, topOfWheel + dropCenterDepth);
    this.drawLine(spokeX, topOfWheel + dropCenterDepth, spokeX + dropCenterWidth, topOfWheel + dropCenterDepth);
    this.drawLine(spokeX + dropCenterWidth, topOfWheel + dropCenterDepth, spokeX + dropCenterWidth + dropCenterInnerAngleWidth, topOfWheel);
    this.drawLine(spokeX + dropCenterWidth + dropCenterInnerAngleWidth, topOfWheel, rightOfWheel, topOfWheel);

    // inner lip
    this.drawLine(rightOfWheel + fudgeFactor, topOfWheel + fudgeFactor, rightOfWheel + fudgeFactor, topOfWheel - lipHeight);
    this.canvasContext.closePath();
  }

  clear() {
    this.canvasContext.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }

  drawRectHeightAndWidth(left: number,  top: number,  width: number,  height: number) {
    this.canvasContext.strokeRect(left,  top,  width,  height);
  }

  fillRectAbsolute(x1: number,  y1: number,  x2: number,  y2: number) {
    this.canvasContext.fillRect(x1,  y1,  x2 - x1,  y2 - y1);
  }

  drawRectAbsolute(x1: number,  y1: number,  x2: number,  y2: number) {
    this.drawRectHeightAndWidth(x1,  y1,  x2 - x1,  y2 - y1);
  }

  drawLine(x1: number,  y1: number,  x2: number,  y2: number) {
    this.canvasContext.moveTo(x1, y1);
    this.canvasContext.lineTo(x2, y2);
    this.canvasContext.stroke();
  }

  drawBezierCurve(beginX: number, beginY: number, controlPoint1x: number, controlPoint1y: number, controlPoint2x: number, controlPoint2y: number, endX: number, endY: number, debug: boolean = false) {
    if (debug) {
      this.canvasContext.fillStyle = "#00FFFF";
      this.canvasContext.fillRect(controlPoint1x - 1, controlPoint1y - 1, 3, 3);
      this.canvasContext.fillStyle = "#FF00FF";
      this.canvasContext.fillRect(controlPoint2x - 1, controlPoint2y - 1, 3, 3);
    }
    this.canvasContext.moveTo(beginX, beginY);
    this.canvasContext.bezierCurveTo(controlPoint1x, controlPoint1y, controlPoint2x, controlPoint2y, endX, endY);
    this.canvasContext.stroke();
  }

  drawArc(centerX: number, centerY: number, radius: number, startAngle: number, endAngleRadians: number, clockwise: boolean) {
    this.canvasContext.arc(centerX, centerY, radius, startAngle, endAngleRadians, !clockwise);
    this.canvasContext.stroke();
  }

  millimetersToPixels(millimeters: number): number {
    return millimeters * this.scaleFactor; // drawing scale factor
  }
  inchesToPixels(inches: number): number {
    return this.millimetersToPixels(inchesToMillimeters(inches));
  }

  static copy(from: Set): Set {
    return new Set(from.component, Wheel.copy(from.wheel), Tire.copy(from.tire), Brake.copy(from.brake), from.scaleFactor);
  }

  setScaleFactor(scaleFactor: number) {
    this.scaleFactor = scaleFactor;
    this.oneInch = this.millimetersToPixels(25.4);
    this.twoInches = 2 * this.oneInch;
    this.threeInches = 3 * this.oneInch;
    this.quarterInch = 0.25 * this.oneInch;
    this.halfInch = 0.5 * this.oneInch;
    this.threeQuartersOfAnInch = 0.75 * this.oneInch;
    this.oneAndAHalfInches = 1.5 * this.oneInch;
    this.oneMillimeter = this.millimetersToPixels(1);
    this.twoMillimeters = this.oneMillimeter * 2;
    this.threeMillimeters = this.oneMillimeter * 3;
    this.fourMillimeters = this.oneMillimeter * 4;
    this.fiveMillimeters = this.oneMillimeter * 5;
  }
}

export class Wheel {
  component: WheelAndTireSizeComponent;
  diameterInches: number;
  widthInches: number;
  offsetMillimeters: number;
  backspacingInches: number;
  backspacingDeltaInches: number;

  constructor(component: WheelAndTireSizeComponent, diameterInches: number, widthInches: number, offsetMillimeters: number) {
    this.component = component;
    this.diameterInches = diameterInches;
    this.widthInches = widthInches;
    this.offsetMillimeters = offsetMillimeters;
    this.recompute();
  }

  recompute() {
    this.backspacingInches = (this.widthInches / 2) + millimetersToInches(this.offsetMillimeters) + 0.47;
    if (this.component.sets.length > 0) {
      this.backspacingDeltaInches = this.backspacingInches - this.component.sets[0].wheel.backspacingInches;
    }
  };

  static copy(from: Wheel): Wheel {
    return new Wheel(from.component, from.diameterInches, from.widthInches, from.offsetMillimeters);
  }
}

export class Tire {
  component: WheelAndTireSizeComponent;
  sectionWidthMillimeters: number;
  profileHeightPercentage: number;
  wheelDiameterInches: number;
  tireDiameterInches: number;
  tireCircumferenceInches: number;
  profileHeightInches: number;
  sizeRatio: number;
  sizeDeltaPercentage: number;
  sameAsBaseline: boolean;

  constructor(component: WheelAndTireSizeComponent, sectionWidthMillimeters: number, profileHeightPercentage: number, wheelDiameterInches: number) {
    this.component = component;
    this.sectionWidthMillimeters = sectionWidthMillimeters;
    this.profileHeightPercentage = profileHeightPercentage;
    this.wheelDiameterInches = wheelDiameterInches;
    this.recompute();
  }

  recompute() {
    this.profileHeightInches = this.sectionWidthMillimeters * (this.profileHeightPercentage/100) / 25.4;
    this.tireDiameterInches = (2 * this.profileHeightInches) + this.wheelDiameterInches;
    this.tireCircumferenceInches = this.tireDiameterInches * Math.PI;

    if (this.component.sets.length > 0) {
      this.sizeRatio = this.tireDiameterInches / this.component.sets[0].tire.tireDiameterInches;
      this.sizeDeltaPercentage = this.sizeRatio - 1;
      this.sameAsBaseline = Math.abs(this.sizeDeltaPercentage) < 0.0001;
    }
  };

  static copy(from: Tire): Tire {
    return new Tire(from.component, from.sectionWidthMillimeters, from.profileHeightPercentage, from.wheelDiameterInches);
  }
}

export class Brake {
  rotorDiameterInches: number;
  rotorThicknessMillimeters: number;
  hatThicknessMillimeters: number;
  hatDiameterInches: number;

  constructor(rotorDiameterInches: number, rotorThicknessMillimeters: number, hatThicknessMillimeters: number, hatDiameterInches: number) {
    this.rotorDiameterInches = rotorDiameterInches;
    this.rotorThicknessMillimeters = rotorThicknessMillimeters;
    this.hatThicknessMillimeters = hatThicknessMillimeters;
    this.hatDiameterInches = hatDiameterInches;
  }

  static copy(from: Brake): Brake {
    return new Brake(from.rotorDiameterInches, from.rotorThicknessMillimeters, from.hatThicknessMillimeters, from.hatDiameterInches);
  }
}
