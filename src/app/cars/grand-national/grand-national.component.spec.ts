import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrandNationalComponent } from './grand-national.component';

describe('GrandNationalComponent', () => {
  let component: GrandNationalComponent;
  let fixture: ComponentFixture<GrandNationalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrandNationalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrandNationalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
