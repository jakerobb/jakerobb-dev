import {Directive} from '@angular/core';

@Directive({
  selector: '[appTwoDecimal]'
})
export class TwoDecimalDirective {
  constructor() {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function (scope, element, attrs, ngModel) {

        ngModel.$formatters.push(function (value) {
          debugger;
          return Math.round(value * 100) / 100;
        });

      }
    }
  }
}
